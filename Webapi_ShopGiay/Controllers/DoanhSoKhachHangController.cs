﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webapi_ShopGiay.Models;

namespace Webapi_ShopGiay.Controllers
{
    public class DoanhSoKhachHangController : ApiController
    {
        [HttpGet]
        public List<DoanhSoModel> getDoanhSoById(int idCustomer, string dateFrom, string dateTo)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            DateTime dtDateFrom = (dateFrom.Equals("null") ? DateTime.MinValue : DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            DateTime dtDateTo = (dateTo.Equals("null") ? DateTime.Now : DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            return db.tbDoanhSoKhachHangs.Where(x => (idCustomer == 0 || x.idKhachHang == idCustomer) 
                                            && (dtDateFrom == DateTime.MinValue || x.NgayLap >= dtDateFrom) 
                                            && (dtDateTo == DateTime.Now || x.NgayLap <= dtDateTo))
                                                .Select(p=>new DoanhSoModel{
                                                id = p.id,
                                                idKhachHang = p.idKhachHang,
                                                ghiChu = p.GhiChu,
                                                doanhSo = p.DoanhSo,
                                                ngayNhap = p.NgayLap
                                                }).OrderByDescending(q=>q.id).ToList();
        }

        [HttpGet]
        public List<DoanhSoModel> getDoanhSoById(string keyword, int idCustomer, string dateFrom, string dateTo)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            DateTime dtdateFrom = (dateFrom.Equals("null") ? DateTime.MinValue : DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            DateTime dtdateTo = (dateTo.Equals("null") ? DateTime.Now : DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            return db.tbDoanhSoKhachHangs.Where(x => string.IsNullOrEmpty(keyword) || x.GhiChu.Contains(keyword) 
                                            && (idCustomer == 0 || x.idKhachHang == idCustomer) 
                                            && (dtdateFrom == DateTime.MinValue || x.NgayLap >= dtdateFrom) 
                                            && (dtdateTo == DateTime.Now || x.NgayLap <= dtdateTo))
                                                .Select(p=>new DoanhSoModel{
                                                id = p.id,
                                                idKhachHang = p.idKhachHang,
                                                ghiChu = p.GhiChu,
                                                doanhSo = p.DoanhSo,
                                                ngayNhap = p.NgayLap
                                                }).OrderByDescending(q=>q.id).ToList();
        }

        [HttpPost]
        public ResultModel insertTurnover(JObject Json)
        {
            ResultModel resultModel;
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbDoanhSoKhachHang dskh = Json.GetValue("turnover").ToObject<tbDoanhSoKhachHang>();
                if (Json.Property("customer") == null)
                {
                    dskh.NgayLap = DateTime.Now;
                    db.tbDoanhSoKhachHangs.InsertOnSubmit(dskh);
                    db.SubmitChanges();
                }
                else
                {
                    tbKhachHang now = Json.GetValue("customer").ToObject<tbKhachHang>();
                    now.NgayNhap = DateTime.Now;
                    now.MaKhachHang = now.TenKhachHang;
                    now.CongNoMax = 0;
                    now.NguongDoanhSoLenVIP = 20000000;
                    now.PhanTramChietKhau = 0;
                    now.TenKhachHangString = UtilsController.BoDauTiengViet(now.TenKhachHang.ToLower());
                    dskh.NgayLap = DateTime.Now;
                    now.tbDoanhSoKhachHangs.Add(dskh);
                    db.tbKhachHangs.InsertOnSubmit(now);
                }
                db.SubmitChanges();
                if (!db.vwKhachHangs.Where(x => x.id == dskh.idKhachHang).FirstOrDefault().isVIP.GetValueOrDefault())
                {
                    if (db.vwKhachHangs.Where(x => x.id == dskh.idKhachHang).FirstOrDefault().TongDoanhSo >= db.tbKhachHangs.Where(x => x.id == dskh.idKhachHang).FirstOrDefault().NguongDoanhSoLenVIP)
                        db.tbKhachHangs.Where(q => q.id == dskh.idKhachHang).FirstOrDefault().isVIP = true;
                }
                db.SubmitChanges();
                resultModel = ResultModel.makeSuccess("Thêm doanh số thành công.");
            }
            catch (Exception ex)
            {
                resultModel = ResultModel.makeSuccess("Thêm doanh số thất bại\nLỗi: "+ ex.Message);
            }
            return resultModel;
        }
        [HttpPut]
        public ResultModel editTurnover(tbDoanhSoKhachHang turnover)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbDoanhSoKhachHang ds = db.tbDoanhSoKhachHangs.FirstOrDefault(x => x.id == turnover.id);
                
                ds.GhiChu = turnover.GhiChu;
                ds.DoanhSo = turnover.DoanhSo;
                db.SubmitChanges();
                if (db.vwKhachHangs.Where(x => x.id == ds.idKhachHang).FirstOrDefault().TongDoanhSo >= db.tbKhachHangs.Where(x => x.id == ds.idKhachHang).FirstOrDefault().NguongDoanhSoLenVIP)
                    db.tbKhachHangs.Where(x => x.id == ds.idKhachHang).FirstOrDefault().isVIP = true;
                else
                    db.tbKhachHangs.Where(x => x.id == ds.idKhachHang).FirstOrDefault().isVIP = false;

                db.SubmitChanges();
                return ResultModel.makeSuccess("Sửa doanh số thành công");
            }
            catch (Exception ex)
            {
                return ResultModel.makeError("Sửa doanh số thất bại.\nLỗi: " + ex.Message);
            }
        }
    }
    public class DoanhSoVM
    {
        public int idKhachHang { get; set; }
        public decimal? tongDoanhSo { get; set; }
    }
    public class DoanhSoModel
    {
        public decimal doanhSo{get;set;}

        public string ghiChu{get;set;}

        public int id { get; set;}

        public int idKhachHang{get;set;}
        public DateTime ngayNhap{get;set;}
    }
}
