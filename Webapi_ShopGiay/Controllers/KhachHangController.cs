﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webapi_ShopGiay.Models;

namespace Webapi_ShopGiay.Controllers
{
    public class KhachHangController : ApiController
    {
        static int limit = 100;
        //get all khach hang
        [HttpGet]
        public List<KhachHangModel> getAllContomers(int page)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangs.OrderByDescending(x => x.NgayLapGanDay).Select(x => new KhachHangModel
            {
                id = x.id,
                MaKhachHang = x.MaKhachHang,
                TenKhachHang = x.TenKhachHang,
                DiaChi = x.DiaChi,
                SoDienThoai = x.SoDienThoai,
                Email = x.Email,
                CongNoDauKy = x.CongNoDauKy,
                SoFAX = x.SoFAX,
                MaSoThue = x.MaSoThue,
                NgayNhap = x.NgayNhap,
                TongDoanhSo = (decimal?)x.TongDoanhSo,
                isVIP = x.isVIP
            }).Skip((page - 1) * limit).Take(limit).ToList();
        }

        //get customer by id
        [HttpGet]
        public List<KhachHangModel> getListCustomer(int page, String keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangs.Where(p => p.TenKhachHangString.Contains(keyword) || p.SoDienThoai.Contains(keyword)).OrderByDescending(x => x.NgayLapGanDay).Select(x => new KhachHangModel
            {
                id = x.id,
                MaKhachHang = x.MaKhachHang,
                TenKhachHang = x.TenKhachHang,
                DiaChi = x.DiaChi,
                SoDienThoai = x.SoDienThoai,
                Email = x.Email,
                CongNoDauKy = x.CongNoDauKy,
                SoFAX = x.SoFAX,
                MaSoThue = x.MaSoThue,
                NgayNhap = x.NgayNhap,
                TongDoanhSo = (decimal?)x.TongDoanhSo,
                isVIP = x.isVIP
            }).Skip((page - 1) * limit).Take(limit).ToList();
        }
        [HttpGet]
        public List<KhachHangModel> getAllCustomers()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangs.OrderByDescending(x => x.NgayLapGanDay).Select(x => new KhachHangModel
            {
                id = x.id,
                MaKhachHang = x.MaKhachHang,
                TenKhachHang = x.TenKhachHang,
                DiaChi = x.DiaChi,
                SoDienThoai = x.SoDienThoai,
                Email = x.Email,
                CongNoDauKy = x.CongNoDauKy,
                SoFAX = x.SoFAX,
                MaSoThue = x.MaSoThue,
                NgayNhap = x.NgayNhap,
                TongDoanhSo = (decimal?)x.TongDoanhSo,
                isVIP = x.isVIP
            }).ToList();
        }

        //get customer by id
        [HttpGet]
        public List<KhachHangModel> getListCustomer(String keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangs.Where(p => p.TenKhachHangString.Contains(keyword) || p.SoDienThoai.Contains(keyword)).OrderByDescending(x=>x.NgayLapGanDay).Select(x => new KhachHangModel
            {
                id = x.id,
                MaKhachHang = x.MaKhachHang,
                TenKhachHang = x.TenKhachHang,
                DiaChi = x.DiaChi,
                SoDienThoai = x.SoDienThoai,
                Email = x.Email,
                CongNoDauKy = x.CongNoDauKy,
                SoFAX = x.SoFAX,
                MaSoThue = x.MaSoThue,
                NgayNhap = x.NgayNhap,
                TongDoanhSo = (decimal?)x.TongDoanhSo,
                isVIP = x.isVIP
            }).ToList();
        }
        
        //add customer
        [HttpPost]
        public ResultModel InsertCustomer(tbKhachHang customer)
        {
            try
            {
                DataStoreDataContext dataStoreDataContext = new DataStoreDataContext();
                customer.NgayNhap = DateTime.Now;
                customer.MaKhachHang = customer.TenKhachHang;
                customer.CongNoMax = decimal.Zero;
                customer.NguongDoanhSoLenVIP = new decimal(20000000);
                customer.PhanTramChietKhau = 0;
                customer.TenKhachHangString = UtilsController.BoDauTiengViet(customer.TenKhachHang.ToLower());
                dataStoreDataContext.tbKhachHangs.InsertOnSubmit(customer);
                dataStoreDataContext.SubmitChanges();
                return ResultModel.makeSuccess("Thêm khách hàng thành công");
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                return ResultModel.makeError(string.Concat("Thêm khách hàng thất bại\nLỗi: ", exception.Message));
            }
        }
        [NonAction]
        public string getIdCustomer()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbKhachHang cus = db.tbKhachHangs.OrderByDescending(p => p.MaKhachHang).First();
            int id = int.Parse(cus.MaKhachHang)+1;
            String maKhachHang =id.ToString();
            int plus = cus.MaKhachHang.Length - maKhachHang.Length;
            for (int i = 0; i < plus; i++)
            {
                maKhachHang = "0" + maKhachHang;
            }

                return maKhachHang;
        }
        
        // update customer
        [HttpPut]
        public ResultModel UpdatetbCustomer(tbKhachHang customer)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                //get customer by id
                tbKhachHang kh = db.tbKhachHangs.FirstOrDefault(x => x.id == customer.id);
                if (kh == null) 
                    return ResultModel.makeError("Có lỗi trong quá trình cập nhật dữ liệu");//if not exist
                kh.TenKhachHang = customer.TenKhachHang;
                kh.TenKhachHangString = UtilsController.BoDauTiengViet(customer.TenKhachHang);
                kh.DiaChi = customer.DiaChi;
                kh.SoDienThoai = customer.SoDienThoai;
                db.SubmitChanges();//confirm
                return ResultModel.makeSuccess("Thay đổi thông tin khách hàng thành công");
            }
            catch (Exception ex)
            {
                return ResultModel.makeError("Lỗi: "+ex.Message);
            }
        }

       
        public class KhachHangModel
        {
            public int id{get; set; }

            public string MaKhachHang{get; set; }

            public string TenKhachHang{get; set; }

            public string DiaChi{get; set; }

            public string SoDienThoai{get; set; }

            public string Email{get; set; }

            public decimal CongNoDauKy{get; set; }

            public string SoFAX{get; set; }

            public string MaSoThue{get; set; }

            public System.DateTime NgayNhap{get; set; }
            public decimal? TongDoanhSo{get; set; }
            public bool? isVIP{get; set; }
        }

    }
}
