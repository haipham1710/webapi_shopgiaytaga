﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Webapi_ShopGiay.Controllers
{
    public class NhanVienController : ApiController
    {


        [HttpGet]
        public IEnumerable<NhanVienVM> GettbNhanVienLists()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbNhanViens.Select(x => new NhanVienVM
            {
                Id = x.id,
                TenDangNhap = x.TenDangNhap,
                TenNhanVien = x.TenNhanVien,
                MatKhau = x.MatKhau,
                ChucVu=x.ChucVu,
                SDT = x.SoDienThoai,
                HopLe = x.HopLe,
            }).AsEnumerable();
        }
        [HttpGet]
        public IEnumerable<NhanVienVM> GettbNhanVienLists(string keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbNhanViens.Where(p=>p.TenNhanVien.Contains(keyword)).Select(x => new NhanVienVM
            {
                Id = x.id,
                TenDangNhap = x.TenDangNhap,
                TenNhanVien = x.TenNhanVien,
                MatKhau = x.MatKhau,
                ChucVu = x.ChucVu,
                SDT = x.SoDienThoai,
                HopLe = x.HopLe,
            }).AsEnumerable();
        }
        [HttpGet]
        public Msg login(string username, string password)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            NhanVienVM nv = db.tbNhanViens.Where(p => p.TenDangNhap.Equals(username)).Select(x => new NhanVienVM
            {
                Id = x.id,
                TenDangNhap = x.TenDangNhap,
                TenNhanVien = x.TenNhanVien,
                MatKhau = x.MatKhau,
                ChucVu = x.ChucVu,
                SDT = x.SoDienThoai,
                HopLe = x.HopLe,
            }).FirstOrDefault();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if(nv !=null)
            {
                if(nv.MatKhau.Equals(password))
                {
                    if(nv.HopLe == true)
                        return new Msg { msg = "Success", idNv = (int)nv.Id };
                    else
                        return new Msg { msg = "Tài khoản đã bị khóa", idNv = 0 };
                }
                else
                    return new Msg { msg = "Sai mật khẩu", idNv = 0 };
            }
            return new Msg { msg = "Không tìm thấy tên đăng nhập này", idNv = 0 };
        }
        
     

      
        public class NhanVienVM
        {
            public decimal Id { get; set; }
            public string TenNhanVien { get; set; }
            public string TenDangNhap { get; set; }
            public string MatKhau { get; set; }
            public string ChucVu { get; set; }
            public string SDT { get; set; }
            public Boolean HopLe { get; set; }
        }
        public class Msg
        {
            public string msg { get; set; }
            public int idNv { get; set; }
        }
  
    }
}

