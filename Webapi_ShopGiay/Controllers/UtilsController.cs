﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Webapi_ShopGiay.Controllers
{
    public class UtilsController : ApiController
    {
        public static string BoDauTiengViet(string accented)
        {
            if (string.IsNullOrEmpty(accented))
            {
                return "";
            }
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string str = accented.Normalize(NormalizationForm.FormD);
            return regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
        }
    }
}
