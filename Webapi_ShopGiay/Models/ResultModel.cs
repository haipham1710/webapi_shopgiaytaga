﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webapi_ShopGiay.Models
{
    public class ResultModel
    {
        public bool success { get; set; }
        public string message { get; set; }
        public static ResultModel makeSuccess(String message)
        {
            return new ResultModel
            {
                success=true,
                message = message
            };
        }
        public static ResultModel makeError(String message)
        {
            return new ResultModel
            {
                success = false,
                message = message
            };
        }
    }
}